﻿namespace WhileStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            if (n == 0 || n == 1)
            {
                return false;
            }

            uint i = 2;
            while (i < n)
            {
                if (n % i == 0)
                {
                    return false;
                }

                i++;
            }

            return true;
        }

        public static uint GetLastPrimeNumber(uint n)
        {
            uint i = 1;
            uint last = 0;
            while (i <= n)
            {
                if (IsPrimeNumber(i))
                {
                    last = i;
                }

                i++;
            }

            return last;
        }

        public static uint SumLastPrimeNumbers(uint n, uint count)
        {
            uint sum = 0;
            while (n > 1 && count != 0)
            {
                if (IsPrimeNumber(n))
                {
                    sum += n;
                    count--;
                }

                n--;
            }

            return sum;
        }
    }
}
