﻿namespace WhileStatements
{
    public static class QuadraticSequences
    {
        public static long SumQuadraticSequenceTerms1(long a, long b, long c, long maxTerm)
        {
            long sum = 0;
            long term = 0;
            int i = 1;
            while (term <= maxTerm)
            {
                term = (a * i * i) + (b * i) + c;
                if (term <= maxTerm)
                {
                    sum += term;
                    i++;
                }
                else
                {
                    i++;
                }
            }

            return sum;
        }

        public static long SumQuadraticSequenceTerms2(long a, long b, long c, long startN, long count)
        {
            long sum = 0;
            int i = 0;
            while (i < count)
            {
                sum += (a * startN * startN) + (b * startN) + c;
                startN++;
                i++;
            }

            return sum;
        }
    }
}
